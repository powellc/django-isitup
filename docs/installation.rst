.. _installation:

Installation
============

* To install ::

    pip install django-pingping

* Add ``'pingping'`` to your ``INSTALLED_APPS`` setting::

    INSTALLED_APPS = (
        # other apps
        "pingping",
    )


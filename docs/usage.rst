.. _usage:

Usage
=====

After installing isitup, there's only a few things to worry about.

Make sure you have an email sending working within your django project.

Also, in your urls add:

```
urlpatterns += patterns('',
    ...
    (r'^<your hook>/', include('isitup.urls')),
    ...
)
```


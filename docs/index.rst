===================
Django Is It Up
===================

Is it up is a fairly straight forward app that takes on the task of checking
web services to see if they are up and running. At the moment, the only goal
is to have it checking http services and sending out emails if an error code
is reached at a specified url.

Development
-----------

The source repository can be found at https://github.com/powellc/django-isitup

Contents
========

.. toctree::
 :maxdepth: 1

 changelog
 installation
 templates
 templatetags
 signals
 usage
 

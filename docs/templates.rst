.. _templates:

Templates
============

* IsItUp uses the following templates, all located in 
  a 'isitup' directory in your TEMPLATE_DIR:

```
service_list.html (/)

service_create.html (/create/)

service_detail.html (/<slug>/)

service_edit.html (/<slug>/edit/)

```
